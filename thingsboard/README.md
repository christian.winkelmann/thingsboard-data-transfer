# Thingsboard Container
See https://thingsboard.io/docs/user-guide/install/docker/ for details.


Run Thingsboard on its own

```shell 
docker-compose up --build
```

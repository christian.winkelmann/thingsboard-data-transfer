# Setup of the thingsboard gateway
This is reduced version of a different repo to reproduce a couple of errors in Thingsboard.
For instance this.

- https://github.com/thingsboard/thingsboard-gateway/issues/790
- https://github.com/thingsboard/thingsboard-gateway/issues/465

Half way fixing it creates a new bug, because the Timestamp in thingsboard seems to be divided by 1000 or sth. else because it ends up to be 1970

![Wrong Datetime](doc/Datetime-bug.png)


## steps to reproduce 
start the containers with 
```` 
docker-compose up --build 
````
You should see a lot of log message in your shell now.

**log into the local running thingboard** instance: http://localhost:8080 as a tenant:
username: tenant@thingsboard.org
password: tenant

**Create a gateway device**: 

![Add Device](doc/AccessToken.png)

at http://localhost:8080/devices
Use this Access Token: oXko2OQcP20GMZgjOP1o

When the data generators run the data there should be no error messages in the logs anymore and the data should end up in the devices overview.

If you want to run the containers forever
```` 
docker-compose up -d --build 
````
To shut them down
```` 
docker-compose down
```` 

### if you need the a self built version of thingsboard
clone thingsboard ( but please in a seperate directory ). There build the thingsboard-gateway. The tag can be used later in the docker file.
````
docker build --tag local_tb-gateway:latest -f docker/Dockerfile ./
````

Check the docker containers with
``` 
docker ps
```
to see if all three containers are working

Use
````
docker logs -f <container-name>
````
to see logs of specific container.

Change the log level in logs.conf to INFO or DEBUG if you want to know more. Usually WARNING should be enough
``` 
docker exec -it tbgateway /bin/bash
```
to get into the thingsboard container with a bash shell. It needs to run, obviously. 


To dive deeper check for logs inside the container
```shell
docker exec -it tbgateway /bin/bash

less / or tail /logs/...
```

If you have changed some configs, restart the docker container:
```
docker restart tb-gateway
```
### setup a database which is used by thingsboard
see [python_data_generator](python_data_generator)

### start thingsboard, mysql and the datasimulator
``` 
docker-compose up --build -d
```

### check the logs of the other containers
```
docker logs -f mysql-db ## look into the mysql container
docker logs -f python-data-generator ## look into the python container
```



## Thingsboard Setup
This is the receiver of messages of thingsboard


Create a Gateway credentials:
https://thingsboard.io/docs/iot-gateway/getting-started/


### shut it down
```
docker-compose down
docker volume prune
docker system prune

```

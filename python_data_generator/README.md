# SLIGHTLY OUTDATED. Only read, if oyu want to know some behind the scences things.

## Zineg to Thingsboard Data Simulator  

To simplify development a script will generate an Mysql Table schema
Another python script will import data into the schema.

### start mysql via docker
https://medium.com/@chrischuck35/how-to-create-a-mysql-instance-with-docker-compose-1598f3cc1bee
```
docker-compose up
```
mysql is running now and you can connect via username:"user" and the password "password"

### create date for functionality testing
With the CREATE_STATEMENT_Functionality.sql you can just create a small table and with the "data_generator.py" you can write a new record every X seconds into it.

### create data for performance testing
#### create the table schema
have a look in "sql_generator.py". This script generates a mysql create table with the amount of columns you specify.

If edited the same amount of colums in "data_generator_huge.py" you can dump many hundred thousand records into the table.

 


### Discusion Performance Testset data with two years of data
When every **two seconds** (or was it 2 Minutes??) a record is written there are

``` 24*3600/2 = 43200 ``` per day

``` 24*3600/2*365 = 15768000 ``` per year

If it is only every **two minutes** but for three cubes

``` 24*60/2*3 =  2160``` per day

``` 24*60/2*365*3 = 788400 ``` per year

#### Size Estimation
With about 50 FLOAT columns and one million rows there are roughly 250MB of space used.

## TODO Read Behaviour
what kind of queries are happening now? Is the database cleaned up after every thingsboard transfer so there are never more than a couple of hundred records in there?
How many reads are there per second? Are the indices right?

## cleanup
### just stop the mysql container and delete the volume
```
docker-compose down
```
delete the volume too
```
docker volume rm sql_my-db
```

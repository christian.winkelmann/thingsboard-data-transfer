"""
setup the schema in mysql to allow writing data into the schema
"""

from random import random
from time import sleep, time

import mysql.connector

try:
    connection = mysql.connector.connect(host='mysqldb', # TODO this refers to the hostname of the mysql database
                                         database='db',
                                         user='user',
                                         password='password')

    with open("CREATE_STATEMENT_Functionality.sql", "r") as f:
        sql_create_statement = f.read()
    mySql_insert_query = f"{sql_create_statement}"
    print(mySql_insert_query)
    cursor = connection.cursor()
    cursor.execute(mySql_insert_query)
    connection.commit()
    cursor.close()

except mysql.connector.Error as error:
    print("Failed to create table {}".format(error))


CREATE TABLE IF NOT EXISTS CUBE_DATA_1 (
    record_id BIGINT NOT NULL AUTO_INCREMENT,
	ts BIGINT NOT NULL,
    temperature float,
    humidity float,
    PRIMARY KEY (record_id)
);

CREATE TABLE IF NOT EXISTS CUBE_DATA_2 (
    record_id BIGINT NOT NULL AUTO_INCREMENT,
	ts TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    temperature float,
    humidity float,
    PRIMARY KEY (record_id)
);

-- ## TODO find an index for bigger datasets to make queries based on Day or month
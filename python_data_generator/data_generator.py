"""
generate data every x seconds and write it to a database
## TODO install "mysql-connector-python" first
https://pynative.com/python-mysql-database-connection/
https://pynative.com/python-mysql-insert-data-into-database-table/
install mysql
"""
import os
from random import random
from time import sleep, time
from loguru import logger
import mysql.connector
import datetime

SLEEP_TIME = int(os.getenv("SLEEP_TIME", 5))
MYSQL_HOST = os.getenv("MYSQL_HOST", "localhost")
RECORDS_PER_INSERT = int(os.getenv("RECORDS_PER_INSERT", 1))


def fill_with_testdata_1():
    while True:


        try:
            connection = mysql.connector.connect(host=MYSQL_HOST, ## see the docker compose configuration
                                                 database='db',
                                                 user='user',
                                                 password='password')
            for i in range(RECORDS_PER_INSERT):
                insert_date = round(time() * 1000)
                temperature = random()*5+20
                humidity = random()*20 + 50

                now = datetime.datetime.now()
                str_now = now.isoformat()

                logger.info(f"datetime: {str_now}")

                mySql_insert_query = f"INSERT INTO CUBE_DATA_1 (ts, temperature, humidity) " \
                                     f"VALUES ({insert_date}, {temperature}, {humidity}) "
                logger.info(mySql_insert_query)
                cursor = connection.cursor()
                cursor.execute(mySql_insert_query)
                connection.commit()
                logger.info(f"{cursor.rowcount} Record inserted successfully into cubes table")
            cursor.close()

        except mysql.connector.Error as error:
            logger.error("Failed to insert record into CUBE_DATA_1 table {}".format(error))

        finally:
            if connection.is_connected():
                connection.close()

    #### write into a second table
        try:
            connection = mysql.connector.connect(host=MYSQL_HOST, ## see the docker compose configuration
                                                 database='db',
                                                 user='user',
                                                 password='password')
            for i in range(RECORDS_PER_INSERT):
                insert_date = round(time() * 1000 )
                temperature = random()*5+20
                humidity = random()*20 + 50

                now = datetime.datetime.now()
                str_now = now.isoformat()
                mySql_insert_query = f"INSERT INTO CUBE_DATA_2 (ts, temperature, humidity) " \
                                     f"VALUES ('{str_now}', {temperature}, {humidity}) "
                logger.info(mySql_insert_query)
                cursor = connection.cursor()
                cursor.execute(mySql_insert_query)
                connection.commit()
                logger.info(f"{cursor.rowcount} Record inserted successfully into cubes table")
            cursor.close()

        except mysql.connector.Error as error:
            logger.error("Failed to insert record into CUBE_DATA_2 table {}".format(error))

        finally:
            if connection.is_connected():
                connection.close()



        sleep(SLEEP_TIME)



if __name__ == '__main__':
    fill_with_testdata_1()